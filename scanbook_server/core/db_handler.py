from .db_base import Session, engine, Base
from .db_models import Book, Comment

class DBHandler:
    def __init__(self):
        Base.metadata.create_all(engine)
        self.session = Session()

    def create_book(self, fidibo_id:int, ISBN:str, title:str, category:str, rating:float, release_date:str,
                 publisher:str, pages:int, picture_url:str, description:str):
        db_book = Book(fidibo_id=fidibo_id, ISBN=ISBN, title=title, category=category,
                       rating=rating, release_date=release_date, publisher=publisher,
                       pages=pages, picture_url=picture_url, description=description)
        self.session.add(db_book)
        self.session.commit()
        return db_book

    def create_comment(self, book_id:int, sender:str, date:str, content:str):
        db_comment = Comment(book_id=book_id, sender=sender, date=date, content=content)
        self.session.add(db_comment)
        self.session.commit()
        return db_comment

    def get_book_by_ISBN(self, ISBN:str):
        # try:
        #     db_book = self.session.query(Book).filter(ISBN=isbn).first()
        # except TypeError as e:
        #     print('Type error:', e)
        #     return None
        db_book = self.session.query(Book).filter_by(ISBN=ISBN).first()
        return db_book

    def get_book_by_id(self, id:int):
        try:
            db_book = self.session.query(Book).filter_by(id=id).first()
        except TypeError:
            return None
        return db_book

    def get_book_comments_by_book_id(self, book_id:int):
        db_comments = self.session.query(Comment).filter_by(book_id=book_id).all()
        return db_comments
