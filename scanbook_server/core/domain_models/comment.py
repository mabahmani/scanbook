

class Comment:
    def __init__(self, book_id:int, sender:str, date:str, content:str):
        self.book_id = book_id
        self.sender = sender
        self.date = date
        self.content = content
