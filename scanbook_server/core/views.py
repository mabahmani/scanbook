from django.shortcuts import render

from scanbook_server.core.use_case.comment_use_case import CommentUseCase
from scanbook_server.crawlers import crawler

# Create your views here.
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from scanbook_server.core.serializers import GetBookSerializer, GetCommentSerializer
from scanbook_server.core.use_case.book_use_case import BookUseCase
import threading
import queue

class BookDetailsAPIView(APIView):
    def get(self, request, ISBN, *args, **kwargs):
        print('ISBN:', ISBN)
        book_use_case = BookUseCase()
        domain_book = book_use_case.get_book_by_ISBN(ISBN=ISBN)
        if not domain_book:
            return Response(data={"error": "book not found!"}, status=status.HTTP_404_NOT_FOUND)

        serializer = GetBookSerializer(instance=domain_book)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class CommentListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        book_id = request.data.get('book_id')
        print('request.data:', request.data)
        if not book_id:
            return Response(data={"error": "book_id is required!"}, status=status.HTTP_400_BAD_REQUEST)

        book_use_case = BookUseCase()
        domain_book = book_use_case.get_book_by_id(id=book_id)
        if not domain_book:
            return Response(data={"error": "book not found!"}, status=status.HTTP_404_NOT_FOUND)

        domain_comments = domain_book.get_comments()
        serializer = GetCommentSerializer(instance=domain_comments, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class InitDatabaseAPIView(APIView):
    workQueue = queue.Queue(1000)
    queueLock = threading.Lock()

    def post(self, request, *args, **kwargs):
        cat = request.data.get('category')
        if not cat:
            return Response(data={"error": "category is required!"}, status=status.HTTP_400_BAD_REQUEST)

        book_page_urls = crawler.get_fidibo_page_books_urls(cat=cat)
        for url in book_page_urls:
            self.workQueue.put(url)
            print(url)
            t = self.MyThread(self.workQueue, self.queueLock)
            t.start()

        return Response(data={"result": "Database filled :)"}, status=status.HTTP_201_CREATED)

    class MyThread(threading.Thread):
        def __init__(self, q: queue.Queue, lock: threading.Lock):
            threading.Thread.__init__(self)
            self.q = q
            self.lock = lock

        def run(self):
            self.lock.acquire()
            if not self.q.empty():
                book_use_case = BookUseCase()
                comment_use_case = CommentUseCase()
                a_book = crawler.get_fidibo_book_details(url=self.q.get())
                if a_book:
                    domain_book = book_use_case.create_book(fidibo_id=a_book.get('fidibo_id'),
                                                            ISBN=a_book.get('ISBN'),
                                                            title=a_book.get('title'),
                                                            category=a_book.get('category'),
                                                            rating=a_book.get('rating'),
                                                            release_date=a_book.get('release_date'),
                                                            publisher=a_book.get('publisher'),
                                                            pages=a_book.get('pages'),
                                                            picture_url=a_book.get('picture_url'),
                                                            description=a_book.get('description'))
                    comments = a_book.get('comments')
                    for a_comment in comments:
                        comment_use_case.create_comment(book_id=domain_book.id,
                                                        sender=a_comment.get('sender'),
                                                        date=a_comment.get('date'),
                                                        content=a_comment.get('content'))
                self.lock.release()
            else:
                self.lock.release()
