
from sqlalchemy import Column, String, Integer, Float, ForeignKey

from sqlalchemy.orm import relationship

from .db_base import Base

class Book(Base):
    __tablename__ = 'books'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    fidibo_id = Column(Integer, nullable=True)
    ISBN = Column(String, nullable=True)
    title = Column(String, nullable=True)
    category = Column(String, nullable=True)
    rating = Column(Float, nullable=True)
    release_date = Column(String, nullable=True)
    publisher =  Column(String, nullable=True)
    pages = Column(Integer, nullable=True)
    picture_url = Column(String, nullable=True)
    description = Column(String, nullable=True)

    comments = relationship('Comment')

    def __init__(self, fidibo_id:int, ISBN:str, title:str, category:str, rating:float, release_date:str,
                 publisher:str, pages:int, picture_url:str, description:str):
        self.fidibo_id = fidibo_id
        self.ISBN = ISBN
        self.title = title
        self.category = category
        self.rating = rating
        self.release_date = release_date
        self.publisher = publisher
        self.pages = pages
        self.picture_url=picture_url
        self.description = description


class Comment(Base):
    __tablename__ = 'comments'
    id = Column(Integer, autoincrement=True, nullable=False, primary_key=True)
    book_id = Column(Integer, ForeignKey('books.id'), nullable=False)

    sender = Column(String, nullable=True)
    date = Column(String, nullable=True)
    content = Column(String, nullable=True)

    def __init__(self, book_id:int, sender:str, date:str, content:str):
        self.book_id = book_id
        self.sender = sender
        self.date = date
        self.content = content