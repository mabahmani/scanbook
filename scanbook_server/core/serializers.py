from rest_framework import serializers

class GetBookSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    fidibo_id = serializers.IntegerField(read_only=True)
    ISBN = serializers.CharField(read_only=True)
    title = serializers.CharField(read_only=True)
    category = serializers.CharField(read_only=True)
    rating = serializers.FloatField(read_only=True)
    release_date = serializers.CharField(read_only=True)
    publisher = serializers.CharField(read_only=True)
    pages = serializers.IntegerField(read_only=True)
    picture_url = serializers.CharField(read_only=True)
    description = serializers.CharField(read_only=True)

class GetCommentSerializer(serializers.Serializer):
    book_id = serializers.IntegerField(read_only=True)
    sender = serializers.CharField(read_only=True)
    date = serializers.CharField(read_only=True)
    content = serializers.CharField(read_only=True)