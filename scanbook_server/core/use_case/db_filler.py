from scanbook_server.core.use_case.book_use_case import BookUseCase
from scanbook_server.core.use_case.comment_use_case import CommentUseCase
from scanbook_server.crawlers import crawler


def fill_database():
    book_use_case = BookUseCase()
    comment_use_case = CommentUseCase()
    book_page_urls = crawler.get_fidibo_page_books_urls()
    for url in book_page_urls:
        a_book = crawler.get_fidibo_book_details(url=url)
        if a_book:
            domain_book = book_use_case.create_book(fidibo_id=a_book.get('fidibo_id'),
                                                    ISBN=a_book.get('ISBN'),
                                                    title=a_book.get('title'),
                                                    category=a_book.get('category'),
                                                    rating=a_book.get('rating'),
                                                    release_date=a_book.get('release_date'),
                                                    publisher=a_book.get('publisher'),
                                                    pages=a_book.get('pages'),
                                                    picture_url=a_book.get('picture_url'),
                                                    description=a_book.get('description'))
            comments = a_book.get('comments')
            for a_comment in comments:
                comment_use_case.create_comment(book_id=domain_book.id,
                                                sender=a_comment.get('sender'),
                                                date=a_comment.get('date'),
                                                content=a_comment.get('content'))

if __name__ == '__main__':
    print('started filling database...')
    fill_database()
    print('database ready!')