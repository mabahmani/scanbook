from scanbook_server.core.db_handler import DBHandler
from scanbook_server.core.domain_models.book import Book


class BookUseCase:

    def create_book(self, fidibo_id:int, ISBN:str, title:str, category:str, rating:float, release_date:str,
                 publisher:str, pages:int, picture_url:str, description:str):
        db_handler = DBHandler()
        db_book = db_handler.create_book(fidibo_id=fidibo_id, ISBN=ISBN, title=title, category=category,
                       rating=rating, release_date=release_date, publisher=publisher,
                       pages=pages, picture_url=picture_url, description=description)

        domain_book = Book(id=db_book.id, fidibo_id=db_book.fidibo_id, ISBN=db_book.ISBN,
                           title=db_book.title, category=db_book.category, rating=db_book.rating,
                           release_date=db_book.release_date, publisher=db_book.publisher,
                           pages=db_book.pages, picture_url=db_book.picture_url,
                           description=db_book.description)

        db_handler.session.close()
        return domain_book

    def get_book_by_ISBN(self, ISBN:str):
        db_handler = DBHandler()
        db_book = db_handler.get_book_by_ISBN(ISBN=ISBN)
        if not db_book:
            return None
        domain_book = Book(id=db_book.id, fidibo_id=db_book.fidibo_id, ISBN=db_book.ISBN,
                           title=db_book.title, category=db_book.category, rating=db_book.rating,
                           release_date=db_book.release_date, publisher=db_book.publisher,
                           pages=db_book.pages, picture_url=db_book.picture_url,
                           description=db_book.description)

        db_handler.session.close()

        return domain_book


    def get_book_by_id(self, id:int):
        db_handler = DBHandler()
        db_book = db_handler.get_book_by_id(id=id)

        if not db_book:
            return None

        domain_book = Book(id=db_book.id, fidibo_id=db_book.fidibo_id, ISBN=db_book.ISBN,
                           title=db_book.title, category=db_book.category, rating=db_book.rating,
                           release_date=db_book.release_date, publisher=db_book.publisher,
                           pages=db_book.pages, picture_url=db_book.picture_url,
                           description=db_book.description)

        db_handler.session.close()

        return domain_book

